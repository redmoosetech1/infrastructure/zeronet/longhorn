# LongHorn
----

```bash
$ kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/v1.4.2/deploy/longhorn.yaml
$ kubectl get pods \
    --namespace longhorn-system \
    --watch
```

# References
----
- https://longhorn.io/docs/1.4.2/deploy/install/install-with-kubectl/
